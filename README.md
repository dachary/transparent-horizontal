Introduction
------------

Un collectif sans structure (au contraire d'un parti politique ou une
entreprise), peut s'auto-organiser par les actions individuelles de
ses membres. Par exemple si chacun agit à chaque fois que la majorité
décide d'une action.

Quand des personnes se mettent d'accord sur des règles de
comportement, l'organisation qui en émerge peut être très
sophistiquée. Le [jeu de la
vie](https://www.youtube.com/watch?v=CgOcEZinQ2I) théorisé par un
mathématicien dans les années 70 montre que des
règles simples produisent des effets complexes. C'est un cas
particulier [d'automate cellulaire](https://fr.wikipedia.org/wiki/Automate_cellulaire),
comparable à un collectif qui se veut horizontal.

Tout comme l'ADN des cellules peut engendrer des formes variées, les
membres d'un collectif qui adoptent des règles de comportement
communes font émerger une organisation.

C'est en partageant un état d'esprit et des intentions que les membres
d'un collectif peuvent s'accorder sur des règles de comportent pour
chercher à faire émerger une organisation. À défaut de règles, le
résultat est imprévisible parcequ'on peut partager des idéaux tout en
se comportant de mille façon différentes.

Actions individuelles qui engendrent transparence et horizontalité
------------------------------------------------------------------

* encourager la divergence des actions et des idées

* rechercher la convergence avec d'autres collectifs, en
  leur faisant des propositions concrètes pour réconcilier des
  actions ou idées divergentes

* débattre des faits objectifs dans un premier temps, puis des idées
  et en dernier lieu des émotions

* ne pas communiquer ou publier une information concernant le
  collectif par un media qui n'est pas accessible à tous aux
  mêmes conditions

* identifier nominativement les membres qui détiennent un pouvoir sur
  le collectif

* partager le pouvoir avec au moins une autre personne

* eviter la collusion des personnes qui détiennent le pouvoir

* demander régulièrement et par vote au collectif l'autorisation de
  continuer à détenir ce pouvoir

* renoncer à un pouvoir si le collectif lui demande par vote

* supprimer toute source de pouvoir, sauf impossibilité

* soumettre au vote toute décision qui engage tout ou partie du collectif

* être respectueux et bienveillant

* faire appel à un tiers en cas de désaccord

* demander de l'aide en cas de doute

* passer la main lorsqu'on s'en va

Qu'est ce qu'un collectif ?
---------------------------

Un collectif est un ensemble de personnes phyisques et morales, avec
ou sans existence légale (association, entreprise, agence
gouvernementale, ...).

Un collectif est composé de sous collectifs, au gré des projets et
affinité de ses membres.

Les mouvements de divergence et de convergence
----------------------------------------------

A chaque fois qu'une idée apparait de nombreux collectifs se forment
au gré de l'imagination de leurs membres (par exemples les dizaines de
collectifs formés en 2016 autour de l'idée de transformer la
démocratie). A l'intérieur même d'un collectif se forment des groupes
qui agissent comme des collectifs de plus petite taille quand ils ont
des avis divergents.

Encourager la divergence d'opinion dans un collectif transparent et
horizontal ne pose pas de problème structurel puisqu'il n'y a pas de
structure. Le désacord, quand il se prolonge peut être vécu comme un
echec parceque c'est habituellement synonyme de problèmes. Mais, même
dans le cas ou quelque membres du collectif agissent dans des
directions opposées, chaque groupe apprend en observant l'autre.

Le collectif y gagne parceque l'action n'est jamais bloquée par
l'attente d'une approbation majoritaire des autres membres. Cela
aténue aussi la compétition entre deux actions opposées: chacune
attire progressivement d'autres membres au fil du temps au lieu de
tout gagner ou perdre sur un seul vote.

Pour parvenir à s'unir à d'autres collectifs ou bien réconcilier des
approches divergentes a l'intérieur d'un collectif, chaque membre doit
faire un effort d'observation objective. Il peut ensuite faire des
propositions concrètes à un autre collectif pour unir leurs idées et
leurs actions. Tout le monde n'a pas le même talent pour la diplomatie
mais il s'agit surtout d'éviter de rester entre soi.

L'engagement personnel et collectif
-----------------------------------

Quand un membre exprime une idée ou entreprend une action, la plupart
du temps il n'engage pas le collectif. Par exemple quand un membre
écrit les règles d'horizontalité et de transparence qu'il essaye de
s'appliquer à lui même, il n'engage pas le collectif. Et même si
plusieurs membres s'accordent sur ces règles, cela n'engage toujours
pas le collectif.

Quand une idée ou une action s'impose à tous les membres, alors le ou
les membres qui en sont responsable engagent le collectif. Imaginons
qu'une fraction des membres aiment l'idée de mettre en place un forum
et que l'un d'eux agit en conséquence. Cela n'engage en rien le
collectif, sauf si le forum en question est accessible depuis la page
de garde du site web du collectif. Les nouveaux venus découvrent le
collectif et pensent naturellement que le forum fait partie des outils
numériques du collectif et non pas qu'il s'agit de l'initiative
indépendante d'un groupe minoritaire. Une discussion archivée sur le
forum sera associée au collectif par le nom de domaine qui apparait
dans l'URL du lecteur extérieur au collectif, etc.

Définition de la transparence
-----------------------------

La transparence est l'accès inconditionel de chaque membre du
collectif à toutes les informations qui le concerne.

Un collectif n'est pas transparent quand:

* les courriels envoyés au contact du collectif ne sont pas archivés
  et consultables.

* des réunions sont organisées par des moyens de communication
  gardés confidentiels

Définition de l'horizontalité
-----------------------------

L'horizontalité est l'absence de délégation de pouvoir à chaque fois
que c'est possible.

Un collectif n'est pas horizontal quand:

* une personne peut parler au nom du collectif et pas les autres

Un collectif est horizontal même quand un membre détient un pouvoir
sur le collectif, à condition que:

* cette information soit facilement accessible aux autres membres du collectif
* moins une autre personne détienne ce pouvoir
* il renonce à ce pouvoir si le collectif lui demande par vote

Quelques exemples de pouvoir:

* l'accès à la gestion du nom de domaine utilisé par le collectif
* l'accès à l'argent du collectif
